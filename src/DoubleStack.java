import java.util.ArrayList;

/**
 * LIFO Stack for processing Reverse Polish Notation.
 *
 * References:
 * https://enos.itcollege.ee/~jpoial/algoritmid/adt.html
 **/

public class DoubleStack {

   public static void main (String[] args) {
   }

   private ArrayList<Double> doubleList;

   /**
    * Standard constructor - creates a new list representing a stack.
    */
   DoubleStack() {
      doubleList = new ArrayList<Double>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack tmp = new DoubleStack();
      if (doubleList.size() > 0)
         for (int i = 0; i <= doubleList.size() - 1; i++)
            tmp.doubleList.add(doubleList.get(i));

      return tmp;
   }

   /**
    * Determine if stack is empty.
    *
    * @return true if stack is empty
    */
   public boolean stEmpty() {
      return (doubleList.isEmpty());
   }

   /**
    * Adds a double into list as the last element.
    *
    * @param a number to be added to the list
    */
   public void push (double a) {
      doubleList.add(a);
   }

   /**
    * Removes last element in stack, if stack is empty throws exception.
    *
    * @return element that was removed.
    */
   public double pop() {
      if (stEmpty())
         throw new IndexOutOfBoundsException();

      double tmp = doubleList.get(doubleList.size() - 1);
      doubleList.remove(doubleList.size() - 1);

      return tmp;
   }

   /**
    * Given an arithmetic expression does operation with the two last elements.
    *
    * @param s arithmetic expression
    */
   public void op (String s) {

      if (doubleList.size() < 2)
         throw new IndexOutOfBoundsException("Too few elements for expression");


      double op1 = pop();
      double op2 = pop();

      switch (s) {
         case "+":
            push(op2 + op1);
            break;
         case "-":
            push(op2 - op1);
            break;
         case "*":
            push(op2 * op1);
            break;
         case "/":
            push(op2 / op1);
            break;
         case "ROT":
            double op3 = pop();
            push(op3);
            push(op2);
            push(op1);
            break;
         case "DUP":
            push(op2);
            push(op1);
            push(op1);
            break;
         case "SWAP":
            push(op1);
            push(op2);
            break;
         default:
            throw new IllegalArgumentException("Invalid operation: " + s);

      }
   }

   /**
    * Reads the top element without removing it.
    *
    * @return top element.
    */
   public double tos() {
      if (stEmpty())
         throw new IndexOutOfBoundsException();

      return doubleList.get(doubleList.size() - 1);
   }

   @Override
   public boolean equals (Object o) {
      if (((DoubleStack) o).doubleList.size() != doubleList.size())
         return false;

      for (int i = 0; i <= doubleList.size() - 1; i++)
         if (!((DoubleStack) o).doubleList.get(i).equals(doubleList.get(i)))
            return false;


      return true;
   }

   @Override
   public String toString() {
      if (stEmpty())
         return "List is empty";

      StringBuffer s = new StringBuffer();
      for (int i = 0; i <= doubleList.size() - 1; i++)
         s.append(doubleList.get(i)).append(" ");

      return s.toString();
   }

   /**
    * Calculate arithmetic expression in Reverse Polish Notation
    *
    * @param pol arithmetic expression
    * @return top element.
    */
   public static double interpret (String pol) {

      double numberInString;
      DoubleStack m = new DoubleStack();

      if (pol == null || pol.isEmpty() || pol.trim().isEmpty())
         throw new NumberFormatException("Arithmetic expression is empty or null: ");

      String removeExtraSpaces = pol.replaceAll("\\s+", " ").trim();
      String[] words = removeExtraSpaces.split("\\s");

      for(String w:words)
         try {
            numberInString = Double.parseDouble(w);
            m.push(numberInString);
         }
         catch (NumberFormatException e)
         {
            if (w.equals("+") || w.equals("-") || w.equals("/") || w.equals("*") || w.equals("ROT") || w.equals("SWAP") || w.equals("DUP"))
               try{
                  m.op(w);
               }
               catch (IndexOutOfBoundsException e1){
                  throw new RuntimeException("Not able to perfom " + w + " in expression " + pol);
               }
            else
               throw new IllegalArgumentException("Illegal symbol: " + w + " in expression " + pol);

         }

      if (m.doubleList.size() != 1)
         throw new RuntimeException("Too many numbers in expression: " + pol);

      return m.tos();
   }

}